FROM alpine:edge

ARG CURRENT_ENV=${CURRENT_ENV}
ARG UNIT_VERSION=1.15.0-r0

RUN apk update \
    && apk upgrade \
    && apk add --no-cache sudo build-base libffi-dev python3-dev postgresql-dev \
        git python3 unit==$UNIT_VERSION unit-python3==$UNIT_VERSION \
    && wget https://bootstrap.pypa.io/get-pip.py \
    && python3 get-pip.py pip==19.0.3 wheel==0.30.0 setuptools==40.8.0 \
    && rm ./get-pip.py

# Создание нового пользователя с ограниченными правами и настройка его прав
# (так как nginx-unit необходимо запускать от root, даем новому пользователю
# запускать его с повышенными привелегиями).
RUN addgroup application \
    && adduser -G application -s /bin/ash -D application \
    && echo "application ALL=(root) NOPASSWD:SETENV: /usr/sbin/unitd" >> /etc/sudoers \
    && echo "Set disable_coredump false" > /etc/sudo.conf

RUN ln -s /usr/bin/python3 /usr/bin/python && \
    ln -s /root/.poetry/bin/poetry /usr/bin/poetry && \
    wget https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py && \
    python3 ./get-poetry.py --version 1.0.2 && \
    poetry config virtualenvs.create false && \
    rm ./get-poetry.py

COPY pyproject.toml poetry.lock /opt/app/
WORKDIR /opt/app/
RUN /bin/ash -c 'poetry install $(test "$CURRENT_ENV" == prod && echo "--no-dev") --no-interaction --no-ansi'
COPY . /opt/app/
RUN chown -R application:application /opt/app/

COPY ./conf_files/nginx_unit/conf.json /var/lib/unit/

USER application
